import os
import site

from setuptools import setup

HERE = os.path.dirname(os.path.abspath(__file__))
site.addsitedir(HERE)


def _get_version():
    from image_cafe import __version__
    return __version__


setup(
    name="image_cafe",
    version=_get_version(),
    author="BrinkerVII",
    author_email="brinkervii@gmail.com",
    packages=["image_cafe"],
    include_package_data=True,
    install_requires=[
        "Click",
        "python-magic",
        "tqdm",
        "rich"
    ],
    entry_points={
        "console_scripts": [
            "image-cafe=image_cafe.main:cli_main"
        ]
    }
)
