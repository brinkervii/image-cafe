import atexit
import hashlib
import logging
import os
import re
import shutil
import signal
import subprocess
import sys
import time
from copy import deepcopy
from dataclasses import dataclass
from logging import LogRecord
from pathlib import Path
from string import Template
from typing import List, Optional, Dict, Tuple

import click
import magic
from rich.console import Console

SCRIPT = Path(__file__).resolve()
HERE = SCRIPT.parent

processes: List[subprocess.Popen] = []

rich_console = Console()


class RichHandler(logging.Handler):
    def emit(self, record: LogRecord) -> None:
        message = record.msg

        match record.levelno:
            case logging.INFO:
                rich_console.print(message, style="green")

            case logging.WARNING:
                rich_console.print(message, style="bold yellow")

            case logging.ERROR:
                rich_console.print(message, style="bold red")

            case logging.DEBUG:
                rich_console.print(message, style="bold blue")

            case _:
                rich_console.print(message)


logger = logging.getLogger()
syslog = RichHandler()
formatter = logging.Formatter('%(asctime)s %(app_name)s : %(message)s')
syslog.setFormatter(formatter)
logger.setLevel(logging.INFO)
logger.addHandler(syslog)

log = logging.LoggerAdapter(logger, {"app_name": "Image Cafe"})


def _home() -> Path:
    if home_string := os.environ.get("HOME", None):
        p = Path(home_string)
        if p.exists():
            return p

    raise RuntimeError("Could not find a home directory")


def _applications() -> Path:
    p = _home() / "Applications"
    p = p.resolve()
    p.mkdir(parents=True, exist_ok=True)

    return p


def _local_share_applications() -> Path:
    return _home() / ".local" / "share" / "applications"


def _image_cafe_apps() -> Path:
    p = _local_share_applications() / "image_cafe" / "apps"
    p = p.resolve()
    p.mkdir(parents=True, exist_ok=True)

    return p


def _icons_directory() -> Path:
    p = _local_share_applications() / "image_cafe" / "icons"
    p = p.resolve()
    p.mkdir(parents=True, exist_ok=True)

    return p


def _mount_app_image(app_image_path: Path) -> Path:
    proc = subprocess.Popen(
        [
            str(app_image_path.resolve()),
            "--appimage-mount"
        ],
        stdout=subprocess.PIPE
    )

    processes.append(proc)

    mount_point = proc.stdout.readline().decode(sys.getdefaultencoding()).rstrip()
    mount_point = Path(mount_point)

    return mount_point


def _update_desktop_database():
    databases = [_local_share_applications()]

    for database in databases:
        log.info(f"Updating desktop database {database}")

        subprocess.check_call([
            "update-desktop-database",
            str(database.resolve())
        ])


def _hash_file(file: Path) -> str:
    h = hashlib.blake2s()

    with file.open("rb") as fp:
        while data := fp.read(4096):
            h.update(data)

    return h.hexdigest().lower()


def _kill_all_processes():
    if not processes:
        return

    log.info(f"We have {len(processes)} processes to kill")

    for proc in processes:
        proc.poll()

        if not proc.returncode:
            proc.send_signal(signal.SIGINT)

    done = False
    t = 0

    log.info(f"Waiting for processes to quit.")
    while not done:
        any(map(lambda p: p.poll(), processes))
        done = all(map(lambda p: p.returncode is not None, processes))
        time.sleep(1)

        t += 1

        if t > 2:
            log.warning(f"Processes are taking a long time to quit, t = {t}")

    processes.clear()


class DesktopFile:
    lines: List[str]
    groups: Dict[Tuple[int, str], Dict[Tuple[int, str], str]]

    _idx: int = 0

    def __init__(
            self,
            lines: Optional[List[str]] = None,
            groups: Optional[Dict[Tuple[int, str], Dict[Tuple[int, str], str]]] = None
    ):
        self.lines = [*(lines or [])]
        self.groups = deepcopy(groups or {})

    def get_value(self, group_needle: str, key_needle: str):
        for (_, group_name), group in self.groups.items():
            if group_name == group_needle:
                for (__, key), value in group.items():
                    if key == key_needle:
                        return value

        return None

    def get_group(self, needle: str, create: Optional[bool] = False):
        for (line_no, group_name), group in self.groups.items():
            if group_name == needle:
                return group

        if create:
            v = self.groups.setdefault((self._idx, needle), dict())
            self._idx += 1

            return v

        return None

    def set_value(self, group_needle: str, key_needle: str, value: str):
        group = self.get_group(group_needle, create=True)

        real_key = None
        for t in group.keys():
            if t[1] == key_needle:
                real_key = t

        if real_key is None:
            real_key = (self._idx, key_needle)
            self._idx += 1

        group[real_key] = value

    def to_file(self, file: Path):
        def sort_key(t):
            return t[0]

        lines = ["# Generated by image_cafe", ""]

        for (_, group_name), group in sorted(self.groups.items(), key=sort_key):
            if len(group) <= 0:
                continue

            lines.append(f"[{group_name}]")

            for (_, key), value in sorted(group.items(), key=sort_key):
                lines.append(f"{key}={value}")

        lines.append("")

        with file.open("w+") as fp:
            fp.write("\n".join(lines))

        file.chmod(0o755)

    @classmethod
    def from_file(cls, file: Path):
        lines = []

        groups = dict()
        active_group = groups.setdefault((-1, "global"), dict())

        line_number = -1
        with file.open("r") as fp:
            while line := fp.readline():
                line_number += 1
                lines.append(line)

                if line.startswith("#"):
                    continue

                elif line.startswith("["):
                    match = re.search(r"^\[(.*)]", line)
                    active_group = groups.setdefault((line_number, match.group(1).strip()), dict())

                elif "=" in line:
                    split = line.split("=")
                    key = (line_number, split[0].strip())
                    value = "=".join(split[1:]).strip()

                    active_group[key] = value

        return cls(lines=lines, groups=groups)


def find_desktop_file(d: Path) -> Optional[Path]:
    return next(d.glob("*.desktop"), None)


def find_icon(d: Path) -> Optional[Path]:
    return next(map(lambda p: p.resolve(), d.glob(".DirIcon")), None)


@click.group()
def cli():
    ...


@dataclass(init=False)
class InstallationInfo:
    app_image_path: Path
    target_app_image_path: Path

    def __init__(self, app_image_path: Path):
        self.app_image_path = app_image_path
        self.target_app_image_path = _applications() / app_image_path.name


@cli.command()
@click.argument("app_image_path", type=Path)
def install(app_image_path: Path):
    log.info(f"Starting app image installation, path = {app_image_path}")

    info = InstallationInfo(app_image_path)

    app_image_stat = app_image_path.stat()
    app_image_mode = app_image_stat.st_mode

    app_image_path.chmod(0o755)
    mount_point = _mount_app_image(app_image_path)

    desktop_path = find_desktop_file(mount_point)
    icon_path = find_icon(mount_point)

    desktop_file = DesktopFile.from_file(desktop_path)
    new_desktop_file = DesktopFile()

    group = "Desktop Entry"
    important_keys = (
        "Name",
        "Exec",
        "Terminal",
        "Type",
        "StartupWMClass",
        "Comment",
        "MimeType",
        "Categories"
    )

    for key in important_keys:
        if value := desktop_file.get_value(group, key):
            new_desktop_file.set_value(group, key, value)

    new_desktop_file.set_value(group, "Exec", str(info.target_app_image_path))
    new_desktop_file.set_value(group, "Version", "1.0")
    app_name = desktop_file.get_value(group, "Name") or ".".join(desktop_path.name.split(".")[:-1])
    app_name = app_name.strip()
    app_hash = _hash_file(app_image_path)

    with icon_path.open("rb") as fp:
        icon_buffer = fp.read(64)

    icon_mime = magic.from_buffer(icon_buffer, mime=True)
    icon_ext = re.search(r"image/(\w+)", icon_mime).group(1).strip()
    new_icon_path = _icons_directory() / f"{app_name.lower()}.{app_hash}.{icon_ext}"
    shutil.copy(icon_path, new_icon_path)
    new_desktop_file.set_value(group, "Icon", str(new_icon_path))

    new_desktop_path = _image_cafe_apps() / f"{app_name.lower()}.desktop"
    new_desktop_file.to_file(new_desktop_path)

    shutil.copy(app_image_path, info.target_app_image_path)
    info.target_app_image_path.chmod(0o755)

    _kill_all_processes()
    app_image_path.chmod(app_image_mode)

    _update_desktop_database()

    log.info("Finished installing AppImage")


@cli.command()
def deploy_desktop():
    target = _home() / ".local" / "share" / "applications" / "image_cafe_install.desktop"

    template = Template("""\
[Desktop Entry]
Version=1.0
Type=Application
Exec=$BIN_PATH install %f
Name=Install with Image Cafe
Icon=system-software-install
Terminal=false
Categories=Utility;
MimeType=application/x-iso9660-appimage;application/x-appimage;application/vnd.appimage;
NoDisplay=true
X-AppImage-Integrate=false
    """)

    desktop_string = template.safe_substitute({
        "BIN_PATH": str(_home() / ".local" / "bin" / "image-cafe")
    })

    with target.open("w+") as fp:
        fp.write(desktop_string)

    target.chmod(0o755)

    desktop = DesktopFile.from_file(target)
    for mime in filter(None, map(str.strip, desktop.get_value("Desktop Entry", "MimeType").split(";"))):
        subprocess.check_call([
            "xdg-mime", "default",
            target.name,
            mime
        ])


@cli.command(name="ls")
def list_installed_images():
    desktop_files = list(
        map(
            lambda p: DesktopFile.from_file(p),
            _image_cafe_apps().glob("*.desktop")
        )
    )

    group = "Desktop Entry"

    print("Installed images with .desktop file:")
    for desktop_file in desktop_files:
        name = desktop_file.get_value(group, "Name")
        image_path = Path(desktop_file.get_value(group, "Exec"))

        print(f"{name} -> {image_path.relative_to(_applications())}")

    print("")
    print("Installed images:")
    for image_path in _applications().glob("*.AppImage"):
        print(image_path.relative_to(_applications()))


def cli_main():
    return cli()


atexit.register(lambda *_, **__: _kill_all_processes())

if __name__ == '__main__':
    cli()
