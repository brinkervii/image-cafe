_HOME = "${HOME}"
PREFIX ?= $(_HOME)/.local
INSTALL_BIN = $(PREFIX)/bin

install: dist/image-cafe
	mkdir -p $(INSTALL_BIN)
	install -Dm755 dist/image-cafe $(INSTALL_BIN)/image-cafe

venv:
	virtualenv venv
	bash -c 'source ./venv/bin/activate; pip install -r requirements.txt; pip install -r requirements.dev.txt'

dist/image-cafe: venv
	bash -c 'source ./venv/bin/activate; pyinstaller -y -F -n image-cafe image_cafe/main.py'

